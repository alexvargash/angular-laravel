@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    <div ng-app="postsRecords" >
                        <div ng-controller="postsController">
                            <!-- Table-to-load-the-data Part -->
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Title</th>
                                        <th>Description</th>
                                        <th><button id="btn-add" class="btn btn-primary btn-xs" ng-click="toggle('add', 0)">Add New Post</button></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr ng-repeat="post in posts">
                                        <td>@{{ post.id }}</td>
                                        <td>@{{ post.title }}</td>
                                        <td>@{{ post.description }}</td>
                                        <td>
                                            <button class="btn btn-default btn-xs btn-detail" ng-click="toggle('edit', post.id)">Edit</button>
                                            <button class="btn btn-danger btn-xs btn-delete" ng-click="confirmDelete(post.id)">Delete</button>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <!-- End of Table-to-load-the-data Part -->
                            <!-- Modal (Pop up when detail button clicked) -->
                            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                                            <h4 class="modal-title" id="myModalLabel">@{{form_title}}</h4>
                                        </div>
                                        <div class="modal-body">
                                            <form name="frmPost" class="form-horizontal" novalidate="">
                                                <div class="form-group">
                                                    <label for="title" class="col-sm-3 control-label">Name</label>
                                                    <div class="col-sm-9">
                                                        <input type="text" class="form-control" id="title" name="title" placeholder="Title" value="@{{title}}" ng-model="post.title" ng-required="true">
                                                        <span class="help-inline" 
                                                        ng-show="frmPost.title.$invalid && frmPost.title.$touched">Title field is required</span>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="description" class="col-sm-3 control-label">Description</label>
                                                    <div class="col-sm-9">
                                                        <input type="text" class="form-control" id="description" name="description" placeholder="Description" value="@{{description}}" ng-model="post.description" ng-required="true">
                                                        <span class="help-inline" 
                                                        ng-show="frmPost.description.$invalid && frmPost.description.$touched">Valid Description field is required</span>
                                                    </div>
                                                </div>
                                            </form>

                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-primary" id="btn-save" ng-click="save(modalstate, id)" ng-disabled="frmPost.$invalid">Save changes</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="{{ asset('app/app.js') }}"></script>
<script src="{{ asset('app/controllers/posts.js') }}"></script>
@endsection
