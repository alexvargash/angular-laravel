app.controller('postsController', function($scope, $http, API_URL) {
    
    // Retrieve posts listing from API
    getPosts();
    
    // Show modal form
    $scope.toggle = function(modalstate, id) {
        $scope.modalstate = modalstate;

        switch (modalstate) {
            case 'add':
                $scope.form_title = "Add New Post";
                $scope.post = {title: '', description: ''};
                break;
            case 'edit':
                $scope.form_title = "Post Detail";
                $scope.id = id;
                $http.get(API_URL + 'posts/' + id)
                    .then(function(response) {
                        console.log(response.data);
                        $scope.post = response.data;
                    });
                break;
            default:
                break;
        }
        console.log(id);
        $('#myModal').modal('show');
    }

    // Save new record / Update existing record
    $scope.save = function(modalstate, id) {
        var url = API_URL + "posts";
        var method = 'POST';
        
        // Append post id to the URL if the form is in edit mode
        if (modalstate === 'edit'){
            url += "/" + id;
            method = 'PATCH';
        }
        
        $http({
            method: method,
            url: url,
            data: $.param($scope.post),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).then(function(response) {
            console.log(response);
            //location.reload();
            getPosts();
        }, function (response) {
            console.log(response);
            alert('This is embarassing. An error has occured. Please check the log for details');
        });
    }

    // Delete one post
    $scope.confirmDelete = function(id) {
        var isConfirmDelete = confirm('Are you sure you want this record?');
        if (isConfirmDelete) {
            $http({
                method: 'DELETE',
                url: API_URL + 'posts/' + id
            }).then(function(data) {
                console.log(data);
                //location.reload();
                getPosts();
            }, function (data) {
                console.log(data);
                alert('Unable to delete');
            });
        } else {
            return false;
        }
    }

    function getPosts() {
       $('#myModal').modal('hide');
       // Retrieve posts listing from API
       $http.get(API_URL + "posts").then(function(response) {
            console.log(response.data);
            $scope.posts = response.data;
        });
    }
});